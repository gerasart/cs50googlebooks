<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28.08.2018
 * Time: 17:16
 */
namespace  Googlebooks;

class Front {

	private $shortcode_name = 'getBooks';

	public function init() {
		add_shortcode( $this->shortcode_name, array( __CLASS__, 'shortCode') );
	}

	public function shortCode() {
		$template = "<div id='getFrontbook'><frontbook></frontbook></div>";
		return $template;
	}
}

