<?php
/**
 * Created by PhpStorm.
 * User: gerasart
 * Date: 28.08.2018
 * Time: 17:15
 */

namespace Googlebooks;

class Scripts {

	public function init() {

			add_action( 'wp_enqueue_scripts', array( __CLASS__, 'incScripts' ) );

	}

	public static function getVersion() {
		$theme = wp_get_theme();

		return $theme->get( 'Version' );
	}

	public function incScripts() {
		$ver = self::getVersion();

		wp_enqueue_script( 'vue', 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.min.js', false, $ver, true );
		wp_enqueue_script( 'currency', plugin_dir_url( '' ) . 'cs50googlebooks/dist/vue/front.js', [ 'vue' ], $ver, true );
        wp_enqueue_style( 'boots', '/wp-content/plugins/cs50googlebooks/backend/css/bootstrap/dist/css/bootstrap.min.css', false, $ver, 'all' );

	}

}