<?php
/**
 * @package GoogleBookAPI
 * @version 1.0
 */
/*
Plugin Name: GoogleBookAPI
Plugin URI: https://cs50.com.ua/resheniya/wordpress-plugins
Description: Developer by <a href="https://cs50.com.ua" >cs50 Gerasart</a> web developers. Use front with vue js. Use shortcode [getBooks] <a href="/wp-admin/plugins.php?page=setting_books">Setting</a>
Version: 1.0
Author URI: https://cs50.com.ua/
*/

include  dirname( __FILE__) . '/front/Scripts.php';
include  dirname( __FILE__) . '/front/Front.php';
include dirname(__FILE__) . '/backend/Setting.php';
include dirname(__FILE__) . '/backend/Actions.php';


use Googlebooks\Scripts as Scripts;
use Googlebooks\Front as Front;
use Googlebooks\Setting as Setting;
use Googlebooks\Actions as Actions;

( new Scripts() )->init();
( new Front() )->init();
( new Setting() )->init();
( new Actions() )->init();




