<?php
/**
 * Created by PhpStorm.
 * User: gerasart
 * Date: 29.08.2018
 * Time: 18:42
 */

namespace Googlebooks;


class Actions {


	public function init() {
		add_action( 'wp_ajax_single_post', array( __CLASS__, 'single_post_insert' ) );
		add_action( 'wp_ajax_nopriv_single_post', array( __CLASS__, 'single_post_insert' ) );
	}

	public static function single_post_insert() {
		$new_post = array(
			'post_title'   => $_POST['title'],
			'post_content' => $_POST['content'],
			'post_status'  => 'publish',
			'post_type'    => 'books'
		);

		$pid = wp_insert_post( $new_post );

		self::testUpload( $_POST['thumbnail'], $pid );

		echo json_encode( array( 'flag' => '1' ) );

		die;
	}


	public static function testUpload($url, $parent_post_id = null) {

		$response = wp_remote_get($url);

		$t  = mktime();
		$filename = 'name' . date( "Y-m-d").$t;


		$image = $response['body'];

		$dir = wp_upload_dir();
		$file = $dir['path'].$filename.".jpeg";

		$fp = fopen($file, "w");
		fwrite($fp, $image);
		fclose($fp);

		$filename =  $file;

		$filetype = wp_check_filetype( basename( $filename ), null );

		$wp_upload_dir = wp_upload_dir();

		$attachment = array(
			'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ),
			'post_mime_type' => $filetype['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
			'post_content'   => '',
			'post_status'    => 'inherit'
		);

		$attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id );

		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
		wp_update_attachment_metadata( $attach_id, $attach_data );

		set_post_thumbnail( $parent_post_id, $attach_id );

	}



}