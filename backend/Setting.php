<?php
/**
 * Created by PhpStorm.
 * User: gerasart
 * Date: 29.08.2018
 * Time: 18:42
 */

namespace Googlebooks;

class Setting {

	private $link;

	public function init() {
		add_action( 'admin_menu', [ $this, 'settingPage' ] );
		$this->link = $_SERVER['REQUEST_URI'];
		$ver = time();
		if ( $this->link === '/wp-admin/plugins.php?page=setting_books' ) {
			wp_enqueue_script( 'vue', 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.min.js', false, $ver, true );
			wp_enqueue_script( 'backend', '/wp-content/plugins/cs50googlebooks/dist/vue/backend.js', [ 'vue' ], '1', true );
			wp_enqueue_style( 'boots', '/wp-content/plugins/cs50googlebooks/backend/css/bootstrap/dist/css/bootstrap.min.css', false, $ver, 'all' );
		}

		add_action( 'init', array( __CLASS__, 'register_post_types' ) );


	}


	public function settingPage() {
		add_submenu_page(
			'plugins.php',
			'Setting books',
			'Setting books',
			'manage_options',
			'setting_books',
			[ $this, 'setting_books' ]
		);
	}

	public function setting_books() {
		global $title;

		print '<div class="wrap">';
		print "<h1>$title</h1>";

		$file = plugin_dir_path( __FILE__ ) . "html/html.php";

		if ( file_exists( $file ) ) {
			require $file;
		}


		print '</div>';
	}

	public function readme_cs50menu_render() {
		return true;
	}

	public static function register_post_types() {

		register_post_type( 'books', array(
			'label'         => null,
			'labels'        => array(
				'name'          => 'Books',
				'singular_name' => __( 'Book' ),
				'menu_name'     => __( 'Books' ),
			),
			'public'        => true,
			'has_archive'   => true,
			'menu_position' => 30,
			'show_in_rest'       => true,
			'menu_icon'     => 'dashicons-book',
			'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'author' ),
			'rewrite'       => array( 'with_front' => false, 'slug' => false ),
			'rest_base'          => 'books',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		) );
	}


}