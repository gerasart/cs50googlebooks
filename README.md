Vue WordPress Plugin
===
		
I created this as a starting point for developing using [VueJS](https://vuejs.org/) within WordPress.

It provides out of the box:
 - VueJS
 - NPM tasks for building and watching for both production and development
 

Getting started for user
---
- git clone to folder plugins this project
- getting setting after activated in plugins/Setting books
- search and save books
- then return in site, use shortcode [getBooks]

Getting started for developer
---

- npm install
- npm build:watch



Note
---
I may not be the greatest with ES6, feel free to fork or issue a pull request if I've written anything against convention.  


